#include <GL/glut.h>
#include "glWindow.h"

int main( int argc, char *argv[] )
{
   glutInitWindowSize( 400, 400 );
   glutInit( &argc, argv );
   glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
   glutCreateWindow( "OpenGL Demonstration 12" );

   /* setup stuff in here */
   glutDisplayFunc( paintWindow );
   glutReshapeFunc( setWindowSize );

   createSurface();

   glutMainLoop();
   return( 0 );
}
