#include <GL/glut.h>
#include "nose.pts"

#define FOVY            40.0
#define MINVIEWDISTANCE 1.0
#define MAXVIEWDISTANCE 1000.0

GLUnurbsObj *nurbSurface;
GLint numPoints;

/* openGL settings */
void setWindowSize( int width, int height )
{
   /* setup the 3D perspective window */
   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   gluPerspective( FOVY, (float)width / (float)height, MINVIEWDISTANCE, MAXVIEWDISTANCE );
   glMatrixMode( GL_MODELVIEW );
   /* make a viewport the acual size of the window */
   glViewport( 0, 0, width, height );
}

void createSurface()
{
   GLfloat materialDiffuse[] = { 0.6, 0.6, 0.6, 1.0 };
   GLfloat materialSpecular[] = { 1.0 ,1.0, 1.0, 1.0 };
   GLfloat materialShininess[] = { 100 };

   /* setup material settings */
   glMaterialfv( GL_FRONT, GL_DIFFUSE, materialDiffuse );
   glMaterialfv( GL_FRONT, GL_SPECULAR, materialSpecular );
   glMaterialfv( GL_FRONT, GL_SHININESS, materialShininess );

   /* enable lighting */
   glEnable( GL_LIGHTING );
   glEnable( GL_LIGHT0 );
   glDepthFunc( GL_LEQUAL );
   glEnable( GL_DEPTH_TEST );
   glEnable( GL_AUTO_NORMAL );
   glEnable( GL_NORMALIZE );

   /* setup nurb */
   nurbSurface = gluNewNurbsRenderer();
   gluNurbsProperty( nurbSurface, GLU_SAMPLING_TOLERANCE, 25.0 );
   gluNurbsProperty( nurbSurface, GLU_DISPLAY_MODE, GLU_FILL );
}

void drawSurface()
{
   GLfloat knots[26] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

   glColor3f( 1.0, 1.0, 1.0 );
   gluBeginSurface( nurbSurface );
   gluNurbsSurface( nurbSurface, 26, knots, 26, knots,
            13*3, 3, &points[0][0][0], 13, 13, GL_MAP2_VERTEX_3 );
   gluEndSurface( nurbSurface );
}

void paintWindow( )
{
   /* clear the buffer */
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   /* no rotations */
   glLoadIdentity();

   /* move the image back from the camera */
   glRotatef( 90, 0.0, 0.0, 1.0 );
   glTranslatef( 10.0, -20.0, -20.0 );
   drawSurface();

   /* swap the back draw buffer to the front */
   glutSwapBuffers( );
}
