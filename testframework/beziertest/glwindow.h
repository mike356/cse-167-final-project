/* openGL settings */
void setWindowSize( int width, int height );

/* window painting */
void paintWindow( void );

/* setup */
void createSurface( void );
void drawSurface( void );

