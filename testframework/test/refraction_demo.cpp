#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Iprogram.h"

using namespace std;
using namespace IFramework;

/*
	This shows how to make skybox with refraction mapping
*/

int main(int argc, char *argv[])
{
  Iprogram::init(argc,argv,"Program Title here",512,512);
  Geode::BOUNDING_BOX = true;
  //Geode::DRAWING_AXISES = true;
  Camera::type = Camera::FREE;

  //Iprogram::sky.load("cubemap_abraham");

  /// Test directional light
  DirectionalLight p(GL_LIGHT0,Vector3(10,10,10));
  Iprogram::world.addChild(&p);
  
  /// Load a Bmp image
  //Iprogram::useSkyBox = true;

  Shader* sh = ShaderManager::addShader("resources/shaders/ocean.vert","resources/shaders/ocean.frag");
  //"","resources/textures/water_normal.bmp","resources/textures/water_height.bmp" 
  GLuint color_id = TextureManager::loadBMP("resources/textures/water.bmp",GL_LINEAR,GL_LINEAR);
  GLuint normal_id = TextureManager::loadBMP("resources/textures/water_normal.bmp",GL_LINEAR,GL_LINEAR);
  GLuint height_id = TextureManager::loadBMP("resources/textures/water_height.bmp",GL_LINEAR,GL_LINEAR);

  for(int i=0;i<10; ++i){
	  for(int j=0;j<10;++j){
		Water* water = new Water(300,1,j*300,0,i*300,color_id,normal_id,height_id);
		water->setShader(sh);
		Iprogram::world.addChild(water);
	  }
  }

  Iprogram::start();

  return 0;
}