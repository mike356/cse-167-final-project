#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Iprogram.h"

using namespace std;
using namespace IFramework;

/*
	The demo demonstrate how to create Terrains
*/
int main(int argc, char *argv[])
{
  Iprogram::init(argc,argv,"First Iprogram",512,512);
  //Geode::BOUNDING_BOX = true;
  //Geode::DRAWING_AXISES = true;
  Camera::type = Camera::FREE;
  Camera::speed_factor = 50;

  /// Test directional light
  // -0.79f, 0.39f, 0.47f
  DirectionalLight p(GL_LIGHT0,Vector3(-0.79f, 0.39f, 0.47f));
  Iprogram::world.addChild(&p);

  Terrain terr1 = Terrain("resources/textures/Terrain2.pgm",257,16,4,true);
  Iprogram::world.addChild(&terr1);

  Terrain terr2 = Terrain(NULL,257,16,4,false);
  terr2.transform.setPosition(0,0,-256*16); /// move the 2nd terrain backward

  //terr1.connector(terr2.vertices);

  Iprogram::world.addChild(&terr2);

  Iprogram::start();

  return 0;
}