#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Iprogram.h"

using namespace std;
using namespace IFramework;

/*
	This demo shows how to create Toon shading with tricky technique to make black outline
*/

int main(int argc, char *argv[])
{
  Iprogram::init(argc,argv,"First Iprogram",512,512);
  Camera::type = Camera::FREE;

  /// Test directional light
  DirectionalLight p(GL_LIGHT0,Vector3(10,10,10));
  Iprogram::world.addChild(&p);

  /// Load a Bmp image
  GLuint tex_id1 = TextureManager::loadBMP("resources/textures/stone_wall.bmp",GL_NEAREST,GL_NEAREST);
  Shader* sh = ShaderManager::addShader("resources/shaders/toon_shading.vert","resources/shaders/toon_shading.frag");

  Sphere* bc = new Sphere(1,10,10,0,0,0);
  bc->drawing_outline = true;
  //bc->selfRotate(Vector3(0,1,0),0,360,2000);
  bc->setTexture(tex_id1);
  bc->setShader(sh);
  //bc->drawing_outline = true;
   Iprogram::world.addChild(bc);

  Iprogram::start();

  return 0;
}