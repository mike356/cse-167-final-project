#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Iprogram.h"

using namespace std;
using namespace IFramework;

/*
	This shows how to make skybox with reflection mapping
	There are errors, need to be fixed
*/

int main(int argc, char *argv[])
{
  Iprogram::init(argc,argv,"Program Title here",512,512);
  //Geode::BOUNDING_BOX = true;
  //Geode::DRAWING_AXISES = true;
  Camera::type = Camera::FREE;
  Iprogram::useSkyBox = true;
  Iprogram::sky.load("cubemap_abraham");

  /// Test directional light
  DirectionalLight p(GL_LIGHT0,Vector3(10,10,10));
  Iprogram::world.addChild(&p);
  
  /// Load a Bmp image
  //GLuint tex_id = TextureManager::loadBMP("resources/textures/earth_2.bmp",GL_NEAREST,GL_NEAREST);
  Shader* sh = ShaderManager::addShader("resources/shaders/reflection.vert","resources/shaders/reflection.frag");
  
  Sphere* cube = new Sphere(10,40,40,0,0,0);
  //cube->transform.selfRotate(0,Vector3(0,1,0),0,360,500);
  cube->setShader(sh);
  cout<<sh->getPid()<<endl;
  Iprogram::world.addChild(cube);

  //Iprogram::sky.texID
  Iprogram::start();

  return 0;
}