/*
Method 1: with moving camera, failed to implement

uniform mat4 ModelMatrix;
uniform vec3 CameraPosition;

varying vec3 ReflectDir;

void main()
{
        vec3 worldPos = vec3( ModelMatrix * gl_Vertex );
        vec3 worldNorm = normalize(vec3(ModelMatrix * vec4(gl_Normal, 0.0)));
        vec3 worldView = normalize( CameraPosition - worldPos );
		
	ReflectDir = reflect(worldView, worldNorm );
	
	gl_Position = ftransform();
}
*/

/// This method doesn't calculate camera position
varying vec3  ReflectDir;
varying float LightIntensity;
uniform vec3 CameraPosition;

void main()
{
    
    vec3 normal    = normalize(gl_NormalMatrix * gl_Normal);
    vec4 pos       = gl_ModelViewMatrix * gl_Vertex;
    vec3 eyeDir    = normalize(pos.xyz);
    ReflectDir     = reflect(eyeDir, normal);
    LightIntensity = max(dot(normalize(gl_LightSource[0].position.xyz - eyeDir), normal),0.0);
	
	gl_Position    = ftransform();
}
