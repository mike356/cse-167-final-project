varying vec4 diffuse,ambientGlobal, ambient;
varying vec3 normal,lightDir,eye;
varying float dist;

void main()
{
	vec4 color = gl_LightModel.ambient ;
	
	float att,NdotL,spotEffect;
    vec3 R = normalize(-reflect(lightDir,normal));
    vec3 E = normalize(-eye);
    
	NdotL = max(dot(normal,lightDir),0.0);

	spotEffect = dot(normalize(gl_LightSource[1].spotDirection),normalize(-lightDir));
	
	if (spotEffect > gl_LightSource[1].spotCosCutoff) {
		/* compute the illumination in here */
	    spotEffect = pow(spotEffect, gl_LightSource[1].spotExponent);
	    	
		att = spotEffect / (gl_LightSource[1].constantAttenuation + gl_LightSource[1].linearAttenuation * dist + gl_LightSource[1].quadraticAttenuation * dist * dist);
	
		color += att*( gl_LightSource[1].ambient*gl_FrontMaterial.ambient);
		color +=  att*(gl_FrontMaterial.diffuse * gl_LightSource[1].diffuse * NdotL);
	
		color +=  att*( gl_FrontMaterial.specular * gl_LightSource[1].specular * pow(max(dot(R,E),0.0),gl_FrontMaterial.shininess) );
	}
		
	gl_FragColor = color;

}