#version 120

varying vec4 normal;
varying vec4 position, viewDirection;

uniform vec3 CameraPosition;

void main()
{
    normal.xyz = normalize(gl_NormalMatrix * gl_Normal);
    normal.w = gl_Vertex.y;
	
	position = gl_ModelViewMatrix * gl_Vertex;
    ///viewDirection =  normalize(CameraPosition - vec3(position));
			   
    gl_TexCoord[0] = gl_MultiTexCoord0;	
    gl_Position = ftransform();
}