	
uniform sampler2D colorTex;
uniform sampler2D normalMap;

varying vec3 LightDir;
varying vec3 ViewDir;

vec3 phongModel( vec3 norm, vec3 diffR ) {

    vec3 r = normalize(reflect( -LightDir, norm ));
    vec4 ambient = gl_LightSource[0].ambient * gl_FrontMaterial.ambient;
    float sDotN = max( dot(normalize(LightDir), norm), 0.0 );
    vec4 diffuse = gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse * vec4(diffR,1) * sDotN;
	
    vec4 spec = vec4(0.0);
    if( sDotN > 0.0 )
        spec = gl_LightSource[0].specular * gl_FrontMaterial.specular *
               pow( max( dot(normalize(r),normalize(ViewDir)), 0.0 ), gl_FrontMaterial.shininess );
			   
    return vec3(diffuse + spec + ambient);
}

void main()
{
    // Lookup the normal from the normal map
    vec3 normal = texture2D( normalMap, gl_TexCoord[0].st ).rgb;
    normal = normalize(normal * 2.0 - 1.0);
    vec4 texColor = texture2D( colorTex, gl_TexCoord[0].st );
	
	gl_FragColor = vec4( phongModel(normal, texColor.rgb), 1.0 );
}
