attribute vec3 tangent;
varying vec3 LightDir;
varying vec3 ViewDir;

void main()
{
	// Building the matrix Eye Space -> Tangent Space
	vec3 norm = normalize (gl_NormalMatrix * gl_Normal);
	vec3 tang = normalize (gl_NormalMatrix * tangent);
	vec3 binormal =  normalize( cross( norm, tang ) );
	 
    // Matrix for transformation to tangent space
    mat3 toObjectLocal = mat3(
        tang.x, binormal.x, norm.x,
        tang.y, binormal.y, norm.y,
        tang.z, binormal.z, norm.z ) ;
		
    // Transform light direction and view direction to tangent space
    vec3 pos = vec3( gl_ModelViewMatrix * gl_Vertex );
    LightDir = normalize( toObjectLocal * (gl_LightSource[0].position.xyz - pos ));

    ViewDir = toObjectLocal * normalize(-pos);	

	gl_TexCoord[0] =  gl_MultiTexCoord0;
		
	gl_Position = ftransform();
}
