#version 120

varying vec4 normal;
varying vec4 position;

void main()
{/*
	gl_TexCoord[0] = gl_Vertex;
	gl_Position = gl_Vertex * 2.0 - 1.0;
	*/
	
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_Position = ftransform();
	
}