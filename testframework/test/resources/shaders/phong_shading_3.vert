varying vec3 normal,lightDir0,lightDir1,lightDir2,lightDir3,lightDir4,eye;
varying float dist0,dist1;

void main()
{
	vec4 ecPos;
	vec3 aux;
	normal = normalize(gl_NormalMatrix * gl_Normal);

	ecPos = gl_ModelViewMatrix * gl_Vertex;
	eye = vec3(ecPos);
		
	aux = vec3(gl_LightSource[0].position-ecPos);
	lightDir0 = normalize(aux);
	dist0 = length(aux);

	aux = vec3(gl_LightSource[1].position-ecPos);
	lightDir1 = normalize(aux);
	dist1 = length(aux);

	aux = vec3(gl_LightSource[2].position-ecPos);
	lightDir2 = normalize(aux);
	
	aux = vec3(gl_LightSource[3].position-ecPos);
	lightDir3 = normalize(aux);
	
	aux = vec3(gl_LightSource[4].position-ecPos);
	lightDir4 = normalize(aux);
					
	gl_Position = ftransform();

}