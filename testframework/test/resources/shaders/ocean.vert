attribute vec3 tangent;
varying vec3 LightDir;
varying vec3 ViewDir;

float rand(vec2 n)
{
  return 0.5 + 0.5 * 
     fract(sin(dot(n.xy, vec2(12.9898, 78.233)))* 43758.5453);
}

void main()
{
	// Building the matrix Eye Space -> Tangent Space
	vec3 norm = normalize (gl_NormalMatrix * gl_Normal);
	vec3 tang = normalize (gl_NormalMatrix * tangent);
	vec3 binormal =  normalize( cross( norm, tang ) );
	 
    // Matrix for transformation to tangent space
    mat3 toObjectLocal = mat3(
        tang.x, binormal.x, norm.x,
        tang.y, binormal.y, norm.y,
        tang.z, binormal.z, norm.z ) ;
		
    // Transform light direction and view direction to tangent space
    vec3 pos = vec3( gl_ModelViewMatrix * gl_Vertex );
    LightDir = normalize( toObjectLocal * (gl_LightSource[0].position.xyz) );
	
    ViewDir = toObjectLocal * normalize(-pos);	

	gl_TexCoord[0] =  gl_MultiTexCoord0;
	//gl_TexCoord[0].s += anim_time*0.1;
	//gl_TexCoord[0].t += anim_time*0.1;
	
	/*
	gl_TexCoord[1] =  gl_MultiTexCoord0;
	gl_TexCoord[1].s += anim_time;
	gl_TexCoord[1].t += anim_time;
	
	vec4 heightColor = texture2D( heightMap, gl_TexCoord[1].st );
	
	vec4 position = gl_Vertex;
	
	position.y = heightColor.r*6.0;
	
	if(position.x == 0.0 || position.x == 120.0){
		position.y = 1.0;
	}
	if(position.z == 0.0 || position.z == 120.0){
		position.y = 1.0;
	}	
	*/
	
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
