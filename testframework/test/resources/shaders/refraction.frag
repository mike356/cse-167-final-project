#version 120
/*
uniform samplerCube CubeMap;
uniform float ReflectFactor;

varying vec3 ReflectDir;


void main()
{
	 vec4 cubeMapColor = textureCube(CubeMap, ReflectDir);
	 gl_FragColor = cubeMapColor ; //mix( gl_FrontMaterial.diffuse, cubeMapColor, ReflectFactor);
	 
	//gl_FragColor = textureCube(CubeMap, reflect(CameraPosition - Position, normalize(Normal)));
}
*/

uniform float ReflectFactor;

uniform samplerCube CubeMap;

varying vec3  ReflectDir;
varying vec3  RefractDir;
varying float LightIntensity;

varying vec3 normal,lightDir,eye;

void main()
{
    // Look up environment map value in cube map
	
    vec4 envColor = textureCube(CubeMap, ReflectDir);
	vec4 refractColor = textureCube(CubeMap, RefractDir);
	
    // Add lighting to base color and mix
	
    vec4 base = LightIntensity * gl_FrontMaterial.diffuse;
	
    envColor  = mix(envColor, base, ReflectFactor);
	
	vec4 refract_reflect_color = mix(refractColor, envColor , 0.1f);
	
    vec3 R = normalize(-reflect(lightDir,normal));
    vec3 E = normalize(-eye);	
	
	float NdotL = max(dot(normal,lightDir),0.0);	
	
	//vec4 color = gl_LightSource[0].ambient*gl_FrontMaterial.ambient;
	///color +=  gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * NdotL;
	
	refract_reflect_color +=  gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(max(dot(R,E),0.0),gl_FrontMaterial.shininess);
	
	gl_FragColor = refract_reflect_color;	
}
