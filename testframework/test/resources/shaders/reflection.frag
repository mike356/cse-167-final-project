/*
Method 1: with moving camera, failed to implement

uniform samplerCube CubeMap;
uniform float ReflectFactor;

varying vec3 ReflectDir;


void main()
{
	 vec4 cubeMapColor = textureCube(CubeMap, ReflectDir);
	 gl_FragColor = cubeMapColor ; //mix( gl_FrontMaterial.diffuse, cubeMapColor, ReflectFactor);
}
*/

uniform float ReflectFactor;

uniform samplerCube CubeMap;

varying vec3  ReflectDir;
varying float LightIntensity;

void main()
{
    // Look up environment map value in cube map

    vec3 envColor = vec3(textureCube(CubeMap, ReflectDir));

    // Add lighting to base color and mix

    vec3 base = vec3(LightIntensity * gl_FrontMaterial.diffuse);
    envColor  = mix(envColor, base, ReflectFactor);

    gl_FragColor = vec4(envColor, 1.0);
}
