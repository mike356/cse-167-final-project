#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Iprogram.h"

using namespace std;
using namespace IFramework;

/*
	Bump mapping Demo
*/
int main(int argc, char *argv[])
{
  Iprogram::init(argc,argv,"First Iprogram",512,512);
  //Geode::BOUNDING_BOX = true;
  //Geode::DRAWING_AXISES = true;
  Camera::speed_factor = 1;
  Camera::type = Camera::FREE;

  PointLight p(GL_LIGHT0,Vector3(30,30,30));
  Iprogram::world.addChild(&p);
  
  /*	
	Create a cube with normal mapping
  */
  Shader* sh = ShaderManager::addShader("resources/shaders/bump_map.vert","resources/shaders/bump_map.frag");
  Cube* cube = new Cube(10,10,10,0,0,0);
  //cube->transform.selfRotate(0,Vector3(0,1,0),0,360,2000);
  cube->setShader(sh);
  cube->setNormalMapping("resources/textures/stone_wall.bmp","resources/textures/stone_wall_normal_map.bmp");
  Iprogram::world.addChild(cube);

  /*
	Create a sphere with normal mapping
  */
  /*
  Shader* sh2 = ShaderManager::addShader("resources/shaders/bump_map.vert","resources/shaders/bump_map.frag");
  Sphere* sp = new Sphere(10,100,100,30,0,0);
  //sp->transform.selfRotate(0,Vector3(0,1,0),0,360,2000);
  sp->setShader(sh2);
  sp->setNormalMapping("resources/textures/earthmap.bmp","resources/textures/earth_normal.bmp");
  Iprogram::world.addChild(sp);
  */

  Iprogram::start();
  return 0;
}