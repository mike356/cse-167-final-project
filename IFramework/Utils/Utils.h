#ifndef _UTILS_H
#define _UTILS_H

#include <Core/Math/Vector3.h>
#include <string.h>
#include <GL/glut.h>

namespace IFramework {

class Utils
{
public:
	static double degreeToRad(double degree);
	static double radToDegree(double rad);
	static void drawGird(int x, int y, double space);
	static void limit(double& value,double min, double max);
	static void anglesToAxes(Vector3 angles, Vector3& left, Vector3& up, Vector3& forward);
	static double random(double a, double b);

	static double ***alloc_data(size_t xlen, size_t ylen, size_t zlen);
	static void free_data(double ***data, size_t xlen, size_t ylen);
	static char* stringToCharPointer(string a);
	static void drawNormalLines(double (*vertices)[3],double (*normals)[3],int size);
	static void spherePoint(double* v, double radius, double theta, double gamma );
};


}


#endif