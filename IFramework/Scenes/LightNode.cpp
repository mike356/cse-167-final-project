#include "LightNode.h"

namespace IFramework {

LightNode::LightNode(){
	position.x = 0;
	position.y = 0;
	position.z = 0;

	matrix.identity();
}

LightNode::LightNode(float x ,float y, float z){
	position.x = x;
	position.y = y;
	position.z = z;

	matrix.identity();
}

void LightNode::draw(Matrix4 & m, Vector3 pos){

	glPushMatrix();

	glMultMatrixd(m.getPointer());
	glTranslated(position.x,position.y,position.z);

	render();

	glPopMatrix();
}

}