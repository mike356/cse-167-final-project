#include "Geode.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

using glm::mat4;

namespace IFramework {


bool Geode::BOUNDING_BOX = false;
bool Geode::DRAWING_AXISES = false;

Geode::Geode(){
	transform = Transform(0,0,0);
	drawing_outline = false;
	drawing_backface = false;
	drawing_doublefaces = false;
	disable_depth = false;
	disable_light = false;

	textureId = 0;
	hasTexture = false;

	shader = NULL;
	hasShader = false;

	material = Material();
	box = BoundingBox();
}

Geode::Geode(double x ,double y, double z){

	transform = Transform(x,y,z);
	drawing_outline = false;
	drawing_backface = false;
	drawing_doublefaces = false;
	disable_depth = false;
	disable_light = false;

	textureId = 0;
	hasTexture = false;

	shader = NULL;
	hasShader = false;

	material = Material();
	box = BoundingBox();
}

void Geode::draw(Matrix4 & m, Vector3 _pos){
  /// update transformation
  transform.update();

  ///Calculate world coordinates of bounding box
  BoundingBox world_box;
  
  Vector3 pos = this->transform.position + _pos;

  Vector4 world_corner(box.corner.x+pos.x,box.corner.y+pos.y,box.corner.z+pos.z,1);
  Vector4 world_max(box.x+pos.x,box.y+pos.y,box.z+pos.z,1);

  world_corner = m*world_corner;
  world_max = m*world_max;

  world_box.setBox(Vector3(world_corner.x,world_corner.y,world_corner.z),world_max.x,world_max.y,world_max.z);

  if (true){//Frustum::boxInFrustum(world_box) != Frustum::OUTSIDE ) {

	if(BOUNDING_BOX){
		drawBoundingBox();
	}

	if(DRAWING_AXISES){
		drawAxises();
	}

	glPushMatrix();
	glMultMatrixd(m.getPointer());
	transform.animate();

	if(disable_light){
		glDisable(GL_LIGHTING);
	}

	if(disable_depth){
		glDisable (GL_DEPTH_TEST);
	}

	if(drawing_doublefaces){
		glDisable(GL_CULL_FACE);
	}

	if(drawing_backface){
		glFrontFace(GL_CW);
	}

	if(hasTexture){
		glActiveTexture(GL_TEXTURE0 + 0);
		glBindTexture(GL_TEXTURE_2D, textureId);
	}

	if(hasShader){
		shader->bind();
		glUniform1i(glGetUniformLocation(shader->getPid(), "tex"), 0);
	}

	material.render();
	
	if(drawing_outline){
		glColor3f(0, 0, 0);
		glCullFace(GL_FRONT);
		glLineWidth(100/(this->transform.position- Camera::position).length());
		glPolygonMode(GL_BACK, GL_LINE);
		render();

		glCullFace(GL_BACK);
		glLineWidth(1.0f);
		glPolygonMode(GL_BACK, GL_FILL);
		glColor3f(1, 1, 1);
		render();
	}else{
		render();
	}

	if(hasShader){
		shader->unbind();
	}

	if(hasTexture){
		glActiveTexture(GL_TEXTURE0 + 0);
		glBindTexture(GL_TEXTURE_2D, NULL);
	}

	if(drawing_backface){
		glFrontFace(GL_CCW);
	}

	if(drawing_doublefaces){
		glEnable(GL_CULL_FACE);
	}

	if(disable_depth){
		glEnable (GL_DEPTH_TEST);
	}

	if(disable_light){
		glEnable(GL_LIGHTING);
	}
	Frustum::drawn++;
	
	glPopMatrix();
  }

}

void Geode::drawBoundingBox(){
	
		//box.print();
		glDisable(GL_LIGHTING);
		glColor3f(1.0, 1.0, 1.0);

		Vector3 pos = this->transform.position;
		Vector3 min = box.corner+pos;
		double max_x = box.x + pos.x;
		double max_y = box.y + pos.y;
		double max_z = box.z + pos.z;

		//// x y plane
		glBegin(GL_LINE_LOOP);
		glVertex3f(min.x,min.y,min.z);
		glVertex3f(max_x,min.y,min.z);
		glVertex3f(max_x,max_y,min.z);
		glVertex3f(min.x,max_y,min.z);
		glEnd();

		/// x z plane
		glBegin(GL_LINE_LOOP);
		glVertex3f(min.x,min.y,min.z);
		glVertex3f(max_x,min.y,min.z);
		glVertex3f(max_x,min.y,max_z);
		glVertex3f(min.x,min.y,max_z);
		glEnd();

		/// z y plane
		glBegin(GL_LINE_LOOP);
		glVertex3f(min.x,min.y,min.z);
		glVertex3f(min.x,min.y,max_z);
		glVertex3f(min.x,max_y,max_z);
		glVertex3f(min.x,max_y,min.z);
		glEnd();

		//// x y plane z
		glBegin(GL_LINE_LOOP);
		glVertex3f(min.x,min.y,max_z);
		glVertex3f(max_x,min.y,max_z);
		glVertex3f(max_x,max_y,max_z);
		glVertex3f(min.x,max_y,max_z);
		glEnd();

		/// x z plane y
		glBegin(GL_LINE_LOOP);
		glVertex3f(min.x,max_y,min.z);
		glVertex3f(max_x,max_y,min.z);
		glVertex3f(max_x,max_y,max_z);
		glVertex3f(min.x,max_y,max_z);
		glEnd();

		/// z y plane x
		glBegin(GL_LINE_LOOP);
		glVertex3f(max_x,min.y,min.z);
		glVertex3f(max_x,min.y,max_z);
		glVertex3f(max_x,max_y,max_z);
		glVertex3f(max_x,max_y,min.z);
		glEnd();
		glEnable(GL_LIGHTING);
}

void Geode::drawAxises(){

	double x = (box.corner.x + box.x)/2;
	double y = (box.corner.y + box.y)/2;
	double z = (box.corner.z + box.z)/2;
	double x_radius = abs(box.x - box.corner.x)/2;
	double y_radius = abs(box.y - box.corner.y)/2;
	double z_radius = abs(box.z - box.corner.z)/2;

	glDisable(GL_LIGHTING);
	glLineWidth(3.0f);
	glBegin(GL_LINES);
		glColor3f(1.0, 0.0, 0.0);
		glVertex3f(x,y,z);
		glVertex3f(x+x_radius+1,y,z);
	glEnd();

	glBegin(GL_LINES);
	    glColor3f(0.0, 1.0, 0.0);
		glVertex3f(x,y,z);
		glVertex3f(x,y+y_radius+1,z);
	glEnd();

	glBegin(GL_LINES);
	    glColor3f(0.0, 0.0, 1.0);
		glVertex3f(x,y,z);
		glVertex3f(x,y,z+z_radius+1);
	glEnd();
	glLineWidth(1.0f);
	glEnable(GL_LIGHTING);
}

void Geode::setTexture(GLuint& id){
	textureId = id;
	hasTexture = true;
}

void Geode::setShader(Shader* sh){
	shader = sh;
	hasShader = true;
}

}