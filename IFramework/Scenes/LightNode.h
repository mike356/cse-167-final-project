#ifndef _LIGHTNODE_H_
#define _LIGHTNODE_H_

#include <Shaders\Shader.h>
#include <Scenes\Node.h>
#include <Objects\BoundingBox.h>
#include <Materials\Material.h>
#include <Cameras\Frustum.h>

namespace IFramework {

class LightNode : public Node
{
private:
	Matrix4 matrix;

  public:
	Vector3 position;

	LightNode();
	LightNode(float x ,float y, float z);

	void init();
    void draw(Matrix4 & m, Vector3 pos);
	void update();

    virtual void render() = 0;
};

}
#endif