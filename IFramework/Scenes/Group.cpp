#include "Group.h"

namespace IFramework {

	Group::Group(){
		transform = Transform();
		hasShader = false;
		hasTexture = false;
		drawing_outline = false;
	}

	void Group::setShader(Shader* sh){
		hasShader = true;
		shader = sh;
	}

	void Group::setTexture(GLuint& id){
		textureId = id;
		hasTexture = true;
	}


	void Group::draw(Matrix4 & m, Vector3 pos)
	{


		std::list<Node*>::iterator nodes;

		glPushMatrix();

		transform.update();
		glMultMatrixd(m.getPointer());
		transform.animate();

		///Matrix4 new_mat = transform.getMatrix()*m;
		
		if(drawing_outline){
			glDisable(GL_LIGHTING);
			glDisable(GL_TEXTURE);
			
			glCullFace(GL_FRONT);
			glLineWidth(4000/(this->transform.position- Camera::position).length());
			glPolygonMode(GL_BACK, GL_LINE);
			glColor3f(0, 0, 0);

			for(nodes = children.begin(); nodes != children.end(); ++nodes)
			{
				(*nodes)->draw(m,transform.position + pos);
			}

			glEnable(GL_LIGHTING);
			glEnable(GL_TEXTURE);
			glCullFace(GL_BACK);
			glLineWidth(1.0f);
			glPolygonMode(GL_BACK, GL_FILL);
			glColor3f(1, 1, 1);

			if(hasTexture){
				glActiveTexture(GL_TEXTURE0 + 0);
				glBindTexture(GL_TEXTURE_2D, textureId);
			}


			if(hasShader){
				shader->bind();
				glUniform1i(glGetUniformLocation(shader->getPid(), "tex"), 0);
			}

			for(nodes = children.begin(); nodes != children.end(); ++nodes)
			{
				(*nodes)->draw(m,transform.position + pos);
			}
		}else{

			if(hasTexture){
				glActiveTexture(GL_TEXTURE0 + 0);
				glBindTexture(GL_TEXTURE_2D, textureId);
			}

			if(hasShader){
				shader->bind();
			}

			for(nodes = children.begin(); nodes != children.end(); ++nodes)
			{
				(*nodes)->draw(m,transform.position + pos);
			}
		}

		glPopMatrix();

		if(hasShader){
			shader->unbind();
		}

		if(hasTexture){
			glActiveTexture(GL_TEXTURE0 + 0);
			glBindTexture(GL_TEXTURE_2D, NULL);
		}

	}



}