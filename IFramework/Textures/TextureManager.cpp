#include "TextureManager.h"

using namespace std;

/// Hopefully, we only need max 8 textures, glActiveTexture and glBindTexture
/// GL_MAX_TEXTURE_UNITS
const int MAX_TEXTURES = 8;

GLuint* TextureManager::textures = new GLuint[MAX_TEXTURES];
GLuint TextureManager::currentIndex = -1;

void TextureManager::init(){
	glGenTextures(MAX_TEXTURES, textures);

}

GLuint TextureManager::loadPNG(char* filename, GLint  gl_mag, GLint gl_min)
{
  std::vector<unsigned char> image; //the raw pixels
  unsigned width, height;

  //decode
  unsigned error = lodepng::decode(image, width, height, filename);

  //if there's an error, display it
  if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

  size_t u2 = 1; while(u2 < width) u2 *= 2;
  size_t v2 = 1; while(v2 < height) v2 *= 2;

  std::vector<unsigned char> image2(u2 * v2 * 4);
  for(size_t y = 0; y < height; y++)
  for(size_t x = 0; x < width; x++)
  for(size_t c = 0; c < 4; c++)
  {
    image2[4 * u2 * y + 4 * x + c] = image[4 * width * y + 4 * x + c];
  }
  
  ++currentIndex;
  //glActiveTexture(GL_TEXTURE0+currentIndex);
  glBindTexture(GL_TEXTURE_2D,textures[currentIndex]);
  
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  glTexImage2D(GL_TEXTURE_2D, 0, 4, u2, v2, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image2[0]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, gl_mag);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, gl_min);
  // GL_LINEAR_MIPMAP_LINEAR;
  // GL_NEAREST;

  return textures[currentIndex];
}

GLuint TextureManager::loadBMP(char* filename, GLint  gl_mag, GLint gl_min)
{
  Image* image = Image::loadBMP(filename);

  ++currentIndex;
  //glActiveTexture(GL_TEXTURE0+currentIndex);
  glBindTexture(GL_TEXTURE_2D, textures[currentIndex]);
  
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, image->width, image->height, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, gl_mag);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, gl_min);

  return textures[currentIndex];
}