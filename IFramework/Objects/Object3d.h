#ifndef _OBJECT3D_H_
#define _OBJECT3D_H_

#include <Scenes\Geode.h>

namespace IFramework {

class Object3d: public Geode
{
	private:
		int numbVertices;
		float *vertices;
		float *normals;
		float *texcoords;
		int numbIndices;
		int *indices;
		bool hasNormals;

	public:
		static enum {OBJ, COLLADA};
		
		Object3d(char* name, int type, bool hasNormals);
		void render();
		void calculateBounding();
		void calculateNormals();
};

}

#endif