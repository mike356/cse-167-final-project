#include "DirectionalLight.h"

namespace IFramework {

DirectionalLight::DirectionalLight(GLenum index, Vector3 pos):Light(index,Vector4(pos.x,pos.y,pos.z,0))
{

}

void DirectionalLight::postRender()
{
}

}