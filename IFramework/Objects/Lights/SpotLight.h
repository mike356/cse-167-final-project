#ifndef _SPOTLIGHT_H_
#define _SPOTLIGHT_H_

#include <Objects\Lights\Light.h>

namespace IFramework {

class SpotLight : public Light
{
private:
    GLfloat spot_direction[4];
    float spot_exponent;
	float spot_cutoff;

public:
	SpotLight(GLenum index, Vector3 pos, Vector3 direction, float cutoff);
	void setDirection(float x, float y, float z);
	void setExponent(float c);
	void setCutoff(float c);
    void postRender();
};

}

#endif