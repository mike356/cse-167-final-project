#ifndef _POINTLIGHT_H_
#define _POINTLIGHT_H_

#include <Objects\Lights\Light.h>

namespace IFramework {

class PointLight : public Light
{
  public:
    PointLight(GLenum index, Vector3 pos);
    void postRender();
};

}

#endif