#include "Cone.h"

namespace IFramework {

Cone::Cone()
{
	radius = 1;
	height = 1;
	slices = 10;
	quad = gluNewQuadric();
	calculateBounding();
}

Cone::Cone(float r, float h, float s, float x, float y, float z):Geode(x,y,z)
{
	radius = r;
	height = h;
	slices = s;
	quad = gluNewQuadric();

	calculateBounding();
}

void Cone::render()
{
  gluQuadricTexture(quad,1);
  gluCylinder(quad,radius,0,height,slices,5);
}

void Cone::calculateBounding()
{
	box.corner = Vector3(-radius,-radius, 0 );
	box.x = radius;
	box.y = radius;
	box.z = height;
}


}