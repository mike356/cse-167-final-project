#ifndef _PLANE_H_
#define _PLANE_H_

#include <Scenes\Geode.h>
#include <Textures\TextureManager.h>

namespace IFramework {

class Plane : public Geode
{
private:
	GLint tangentLocation;
	GLuint colorTexId;
	GLuint normalMapId;
	bool passingTangents;

  public:
    Plane();
    Plane(float size,float grid_size,float x,float y,float z);
	float size,grid_size;

	void setNormalMapping( char* colorFile, char* normalMap);
	void calculateBounding();
    void render();
};

}

#endif