#ifndef _BezierPatch_H_
#define _BezierPatch_H_

#include <Shaders\ShaderManager.h>
#include <Textures\TextureManager.h>

#include <Core\Math\Vector3.h>
#include <Scenes\Geode.h>
#include <GL/glut.h>
#include <iostream>
#include <time.h>
#include <Utils\Utils.h>

namespace IFramework{

class BezierPatch: public Geode{

private:
	GLUnurbsObj *theNurb;
	float wave_1;
	float wave_2;
	GLfloat ctlpoints[8][8][3];
	GLfloat ctlpoints2[8][8][3];
	int size,grid_size,height;
	float* knots;

public:
	BezierPatch(const int size,const int grid_size,const int height);

	void calculateBounding();
	void render();

};

}

#endif