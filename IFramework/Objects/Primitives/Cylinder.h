#ifndef _CYLINDER_H_
#define _CYLINDER_H_

#include <Scenes\Geode.h>

namespace IFramework {

class Cylinder : public Geode
{
  private:
	GLUquadric *quad;

  public:
    Cylinder();
    Cylinder(float base_radius, float top_radius, float height , int slices, float x,float y,float z);

	float t_radius, b_radius, height;
	int slices;
	void calculateBounding();
    void render();
};

}

#endif