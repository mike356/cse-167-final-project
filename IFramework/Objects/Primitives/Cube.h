#ifndef _CUBE_H_
#define _CUBE_H_

#include <Scenes\Geode.h>
#include <Textures\TextureManager.h>

namespace IFramework {

class Cube : public Geode
{
  private:
	GLint tangentLocation;
	GLuint colorTexId;
	GLuint normalMapId;
	bool passingTangents;

  public:
    Cube();
    Cube(float width,float height,float thick,float x,float y,float z);
	float width,height,thick;

	void setNormalMapping( char* colorFile, char* normalMap);
	void calculateBounding();
    void render();
};

}

#endif