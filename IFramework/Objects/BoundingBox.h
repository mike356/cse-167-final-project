#ifndef _BOUNDINGBOX_
#define _BOUNDINGBOX_

#include <Core\Math\Vector3.h>

namespace IFramework {

class BoundingBox 
{

public:
	Vector3 corner;
	double x,y,z;
	double width, height, thick;

	BoundingBox::BoundingBox( Vector3 &corner, double x, double y, double z);
	BoundingBox::BoundingBox(void);
	BoundingBox::~BoundingBox();

	void BoundingBox::setBox( Vector3 &corner, double x, double y, double z);

	// for use in frustum computations
	Vector3 BoundingBox::getVertexP(Vector3 &normal);
	Vector3 BoundingBox::getVertexN(Vector3 &normal);
	void print();

};

}
#endif