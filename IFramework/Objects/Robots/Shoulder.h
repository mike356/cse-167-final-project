#ifndef _Shoulder_H_
#define _Shoulder_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cube.h>
#include <Objects\Primitives\Sphere.h>

namespace IFramework {

class Shoulder: public Group
{
	public:
		Vector3 start,end;
		Shoulder(double scale,int type);
};

}

#endif