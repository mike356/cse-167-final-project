#include "Hand.h"
#include <iostream>

using namespace std;

namespace IFramework {
/*
	Types = 1
*/
	Hand::Hand(double scale,int type,bool reflect){

		double w = scale;
		double h = scale;
		double t = scale*0.5;
		int slices = 10;

		int numb_fingers = 3;
		int nodes = 2;
		double finger_radius = scale*0.4;
		double finger_height = scale*1.3;

		Cube* palm = new Cube(w,h,t,0,0,0);
		palm->transform.setAngle(Vector3(0,1,0),90);
		this->addChild(palm);

		for(int i=0; i < numb_fingers ; ++i){
			Cylinder* finger = new Cylinder(finger_radius,finger_radius,finger_height,slices,0,h,i*(finger_radius*2)-2*finger_radius);
			finger->transform.setAngle(Vector3(1,0,0),-90);
			this->addChild(finger);
		}
		
		for(int i=0; i < numb_fingers ; ++i){
			Cylinder* finger = new Cylinder(finger_radius,finger_radius,finger_height,slices,0,h+finger_height,i*(finger_radius*2)-2*finger_radius);
			finger->transform.setAngle(Vector3(0,1,0),80);
			this->addChild(finger);
		}

		double thumb_dist = 0;
		thumb_dist = reflect ? -w : w;

		Cylinder* finger = new Cylinder(finger_radius,finger_radius,finger_height,slices,0,h,thumb_dist);
		finger->transform.setAngle(Vector3(0,1,0),80);
		this->addChild(finger);

	}
}