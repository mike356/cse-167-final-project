#ifndef _LIMB_H_
#define _LIMB_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <ctime>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cylinder.h>
#include <Objects\Primitives\Cone.h>
#include <Objects\Primitives\RevolvedSurface.h>

namespace IFramework {

class Limb: public Group
{
	public:	
		Vector3 start,end;
		Limb(double scale, int type);
};

}

#endif