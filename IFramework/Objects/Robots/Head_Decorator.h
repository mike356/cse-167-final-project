#ifndef _Head_Decorator_H_
#define _Head_Decorator_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cylinder.h>
#include <Objects\Primitives\Sphere.h>
#include <Objects\Primitives\Cube.h>

namespace IFramework {

class Head_Decorator: public Group
{
	public:
		Head_Decorator(double scale,int type);
};

}

#endif