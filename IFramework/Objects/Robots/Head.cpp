#include "Head.h"
#include <iostream>

using namespace std;

/*
	Types: 4
*/
namespace IFramework {
	Head::Head(double scale,int type){

		Geode* geometry;
		double r = scale*2.6;
		int slices = 20;
		int stacks = 20;
		double d = r/sqrt(2.0);

		switch(type){
			case 0:
				geometry = new Sphere(r,slices,stacks,0,0,0);
				
				eyes_pos = Vector3(0,0,r);
				decorator_pos = Vector3(0,d,0);

				this->addChild(geometry);
			break;
			case 1:
				geometry = new Cube(r,r,r,0,0,0);

				eyes_pos = Vector3(0,0,r);
				decorator_pos = Vector3(0,d,0);
				this->addChild(geometry);
			break;
			case 2:
				geometry = new Cylinder(r,r,r*2.5,slices,0,0,0);
				geometry->transform.setAngle(Vector3(1,0,0),90);
				geometry->transform.setPosition(0,r,0);
				eyes_pos = Vector3(0,0,r);
				decorator_pos = Vector3(0,d,0);

				this->addChild(geometry);
			break;
			case 3:
				HemiSphere* sp1 = new HemiSphere(r,slices,slices,r/2,0,0);
				HemiSphere* sp2 = new HemiSphere(r,slices,slices,-r/2,0,0);
				sp1->transform.setAngle(Vector3(0,1,0),-90);
				sp2->transform.setAngle(Vector3(0,1,0),90);

				geometry = new Cylinder(r,r,r,slices,0,0,0);
				geometry->transform.setAngle(Vector3(0,1,0),90);
				geometry->transform.setPosition(-r/2,0,0);
				eyes_pos = Vector3(0,0,r);
				decorator_pos = Vector3(0,d,0);

				this->addChild(geometry);
				this->addChild(sp1);
				this->addChild(sp2);
			break;
		}
	}
}