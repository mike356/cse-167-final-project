#ifndef _Joint_H_
#define _Joint_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <ctime>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cube.h>
#include <Objects\Primitives\Sphere.h>

namespace IFramework {

class Joint: public Group
{
	public:
		Vector3 start,end;
		Joint(double scale);
};

}

#endif