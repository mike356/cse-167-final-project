#include "Joint.h"
#include <iostream>

using namespace std;

/*
	Types: 1
*/
namespace IFramework {

	Joint::Joint(double scale){

		Geode* geometry;
		double r = scale;
		int slices = 6;
		int stacks = 6;

		geometry = new Sphere(r,slices,stacks,0,0,0); 
		end = Vector3(0,r,0);
		start = Vector3(0,-r,0);
		this->addChild(geometry);
	}
}