#include "Robot.h"
#include <iostream>

using namespace std;

/*
	Types: 1
*/
namespace IFramework {
	Robot::Robot(){

		int body_rand = rand()%2;
		int back_rand = rand()%2;
		int limbs_rand = 1;//rand()%2;
		int head_rand = rand()%4;
		int head_decor_rand = rand();
		int shoulder_rand = rand()%2;
		int eyes_rand = rand();

		double scale = 10;

		Body* body = new Body(scale*4,body_rand);
		this->addChild(body);

		/*
		   Back
		*/
		Back* back = new Back(scale*4,back_rand);
		body->addChild(back);

		back->transform.setPosition(body->back_pos.x,body->back_pos.y,body->back_pos.z );
		
		/*
			Arms
		*/
		Shoulder* l_shoulder = new Shoulder(scale,shoulder_rand);
		l_shoulder->transform.selfRotate(150,Vector3(1,-0.2,0),150,200,200,false);
		Limb* ul_arm = new Limb(scale,limbs_rand);

		Joint* jl_arm = new Joint(scale);
		Limb* ll_arm = new Limb(scale,limbs_rand);
		jl_arm->transform.selfRotate(300,Vector3(1,0,0),300,360,200,false);

		Shoulder* r_shoulder = new Shoulder(scale,shoulder_rand);
		r_shoulder->transform.selfRotate(-150,Vector3(1,0.2,0),-210,-150,200,false);

		Limb* ur_arm = new Limb(scale,limbs_rand);
		Joint* jr_arm = new Joint(scale);
		Limb* lr_arm = new Limb(scale,limbs_rand);
		jr_arm->transform.selfRotate(360,Vector3(1,0,0),300,360,200,false);

		body->addChild(l_shoulder);
		body->addChild(r_shoulder);

		l_shoulder->addChild(ul_arm);
		ul_arm->addChild(jl_arm);
		jl_arm->addChild(ll_arm);

		r_shoulder->addChild(ur_arm);
		ur_arm->addChild(jr_arm);
		jr_arm->addChild(lr_arm);

		l_shoulder->transform.setPosition(body->l_arm_pos.x,body->l_arm_pos.y+scale/2, body->l_arm_pos.z);
		r_shoulder->transform.setPosition(body->r_arm_pos.x,body->r_arm_pos.y+scale/2, body->r_arm_pos.z);

		ul_arm->transform.setPosition(l_shoulder->end.x,l_shoulder->end.y,l_shoulder->end.z);
		ur_arm->transform.setPosition(r_shoulder->end.x,r_shoulder->end.y,r_shoulder->end.z);

		jl_arm->transform.setPosition(ul_arm->end.x,ul_arm->end.y+scale/2,ul_arm->end.z);
		jr_arm->transform.setPosition(ur_arm->end.x,ur_arm->end.y+scale/2,ur_arm->end.z);

		ll_arm->transform.setPosition(jl_arm->end.x,jl_arm->end.y-scale/2,jl_arm->end.z);
		lr_arm->transform.setPosition(jr_arm->end.x,jr_arm->end.y-scale/2,jr_arm->end.z);

		/*
			Hands
		*/
		Joint* jl_hand = new Joint(scale);
		Hand* l_hand = new Hand(scale,0,true);

		Joint* jr_hand = new Joint(scale);
		Hand* r_hand = new Hand(scale,0,false);
		r_hand->transform.setAngle(Vector3(0,1,0),180);

		ll_arm->addChild(jl_hand);
		lr_arm->addChild(jr_hand);

		jl_hand->transform.setPosition(ll_arm->end.x,ll_arm->end.y,ll_arm->end.z);
		jr_hand->transform.setPosition(lr_arm->end.x,lr_arm->end.y,lr_arm->end.z);

		jl_hand->addChild(l_hand);
		jr_hand->addChild(r_hand);

		l_hand->transform.setPosition(jl_hand->end.x,jl_hand->end.y,jl_hand->end.z);
		r_hand->transform.setPosition(jr_hand->end.x,jr_hand->end.y,jr_hand->end.z);

		/*
			Head
		*/
		Neck* neck = new Neck(scale);
		body->addChild(neck);

		neck->transform.setPosition(body->head_pos.x,body->head_pos.y-scale/2,body->head_pos.z);
		
		Head* head = new Head(scale,head_rand);
		head->transform.setPosition(neck->end.x,neck->end.y+scale*1.5, neck->end.z);
		head->transform.selfRotate(0,Vector3(0,1,0),-45,45,400,false);
		neck->addChild(head);

		Eyes* eyes = new Eyes(scale,eyes_rand);
		eyes->transform.setPosition(head->eyes_pos.x,head->eyes_pos.y,head->eyes_pos.z);
		head->addChild(eyes);

		Head_Decorator* h_dec = new Head_Decorator(scale,head_decor_rand);
		h_dec->transform.setPosition(head->decorator_pos.x,head->decorator_pos.y,head->decorator_pos.z);
		head->addChild(h_dec);

		/*
			Legs
		*/

		Joint* ujl_leg = new Joint(scale*1.3);
		ujl_leg->transform.selfRotate(150,Vector3(1,0,0),150,190,200,false);

		Limb* ul_leg = new Limb(scale*1.3,limbs_rand);
		Joint* ljl_leg = new Joint(scale*1.3);
		Limb* ll_leg = new Limb(scale*1.3,limbs_rand);
		ljl_leg->transform.selfRotate(10,Vector3(1,0,0),10,40,200,false);

		Joint* ujr_leg = new Joint(scale*1.3);
		ujr_leg->transform.selfRotate(-170,Vector3(1,0,0),-210,-170,200,false);

		Limb* ur_leg = new Limb(scale*1.3,limbs_rand);
		Joint* ljr_leg = new Joint(scale*1.3);
		Limb* lr_leg = new Limb(scale*1.3,limbs_rand);
		ljr_leg->transform.selfRotate(40,Vector3(1,0,0),10,40,200,false);

		body->addChild(ujl_leg);
		body->addChild(ujr_leg);

		ujl_leg->transform.setPosition(body->l_leg_pos.x,body->l_leg_pos.y-scale/2, body->l_leg_pos.z);
		ujr_leg->transform.setPosition(body->r_leg_pos.x,body->r_leg_pos.y-scale/2, body->r_leg_pos.z);

		ujl_leg->addChild(ul_leg);
		ujr_leg->addChild(ur_leg);
		ul_leg->transform.setPosition(ujl_leg->end.x,ujl_leg->end.y-scale/2,ujl_leg->end.z);
		ur_leg->transform.setPosition(ujr_leg->end.x,ujr_leg->end.y-scale/2,ujr_leg->end.z);

		ul_leg->addChild(ljl_leg);
		ur_leg->addChild(ljr_leg);
		ljl_leg->transform.setPosition(ul_leg->end.x,ul_leg->end.y,ul_leg->end.z);
		ljr_leg->transform.setPosition(ur_leg->end.x,ur_leg->end.y,ur_leg->end.z);

		ljl_leg->addChild(ll_leg);
		ljr_leg->addChild(lr_leg);
		ll_leg->transform.setPosition(ljl_leg->end.x,ljl_leg->end.y-scale,ljl_leg->end.z);
		lr_leg->transform.setPosition(ljr_leg->end.x,ljr_leg->end.y-scale,ljr_leg->end.z);

		/*
			Feet
		*/
		Joint* fjl_leg = new Joint(scale);
		fjl_leg->transform.selfRotate(-10,Vector3(1,0,0),-10,50,200,false);

		Foot* l_foot = new Foot(scale);

		Joint* fjr_leg = new Joint(scale);
		fjr_leg->transform.selfRotate(50,Vector3(1,0,0),-10,50,200,true);

		Foot* r_foot = new Foot(scale);

		ll_leg->addChild(fjl_leg);
		lr_leg->addChild(fjr_leg);
		
		fjl_leg->transform.setPosition(ll_leg->end.x,ll_leg->end.y,ll_leg->end.z);
		fjr_leg->transform.setPosition(lr_leg->end.x,lr_leg->end.y,lr_leg->end.z);

		fjl_leg->addChild(l_foot);
		fjr_leg->addChild(r_foot);

		l_foot->transform.setPosition(fjl_leg->end.x,fjl_leg->end.y-scale/2,fjl_leg->end.z);
		r_foot->transform.setPosition(fjr_leg->end.x,fjr_leg->end.y-scale/2,fjr_leg->end.z);
	
	}

}