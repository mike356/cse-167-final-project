#ifndef _NECK_H_
#define _NECK_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cylinder.h>

namespace IFramework {

class Neck: public Group
{
	public:
		Vector3 start,end;
		Neck(double scale);	
};

}

#endif