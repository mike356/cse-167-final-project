#include "Shoulder.h"
#include <iostream>

using namespace std;

/*
	Types: 2
*/
namespace IFramework {

	Shoulder::Shoulder(double scale,int type){

		Geode* geometry;
		double r = scale*2;
		int slices = 6;
		int stacks = 6;

		switch(type){
			case 0:
				geometry = new Sphere(r,slices,stacks,0,0,0); 
				geometry->transform.setPosition(0,r/2,0);
				end = Vector3(0,r,0);
				start = Vector3(0,-r,0);
				this->addChild(geometry);
			break;
			case 1:
				geometry = new Cube(r,r*0.5,r,0,0,0); 
				geometry->transform.setPosition(0,r/2,0);
				end = Vector3(0,r,0);
				start = Vector3(0,-r,0);
				this->addChild(geometry);
			break;
		}
	}
}