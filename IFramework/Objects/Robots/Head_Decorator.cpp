#include "Head_Decorator.h"
#include <iostream>

using namespace std;

/*
	Types: 1
*/
namespace IFramework {
	Head_Decorator::Head_Decorator(double scale,int type){

		double r = scale;

		switch(type){
			case 0:
				Cube* cb1 = new Cube(r*0.1,r*2,r*0.1 ,r,r,0);
				Cube* cb2 = new Cube(r*0.1,r*2,r*0.1 ,-r,r,0);
				cb1->transform.setAngle(Vector3(0,0,1),-30);
				cb2->transform.setAngle(Vector3(0,0,1),30);

				this->addChild(cb1);
				this->addChild(cb2);
			break;
		}
	}
}