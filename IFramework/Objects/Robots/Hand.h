#ifndef _Hand_H_
#define _Hand_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <ctime>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cylinder.h>
#include <Objects\Primitives\Cone.h>
#include <Objects\Primitives\Cube.h>

namespace IFramework {

class Hand: public Group
{
	public:	
		Hand(double scale, int type,bool reflect);
};

}

#endif