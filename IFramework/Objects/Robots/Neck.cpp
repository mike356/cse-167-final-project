#include "Neck.h"
#include <iostream>

using namespace std;

/*
	Types: 1
*/
namespace IFramework {

	Neck::Neck(double scale){

		Geode* geometry;
		double b_r = scale*1.5;
		double t_r = scale*1.5;
		double h = scale*1.5;
		double slices = 6;

		geometry = new Cylinder(b_r,t_r,h,slices,0,0,0); 
		geometry->transform.setAngle(Vector3(1,0,0),-90);

		end = Vector3(0,h,0);
		start = Vector3(0,0,0);
		this->addChild(geometry);
	}
}