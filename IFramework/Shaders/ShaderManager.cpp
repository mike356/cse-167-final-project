#include "ShaderManager.h"

namespace IFramework{

	vector<Shader*> ShaderManager::shaders;
	Shader* ShaderManager::addShader(const char* vertexShader,const char* fragmentShader){
		
		Shader* shader = new Shader(vertexShader,fragmentShader);
		shaders.push_back(shader);

		return shader;
	}

	void ShaderManager::removeShader(Geode* model){
		//delete model->shader;
	}
}