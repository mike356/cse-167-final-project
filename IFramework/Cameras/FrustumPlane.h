#ifndef _FRUSTUM_PLANE_H_
#define _FRUSTUM_PLANE_H_

#include <Core\Math\Vector3.h>

namespace IFramework {

class FrustumPlane 
{
private:

public:
	Vector3 normal,point;
	double d;

	FrustumPlane::FrustumPlane( Vector3 &v1,  Vector3 &v2,  Vector3 &v3);
	FrustumPlane::FrustumPlane();
	FrustumPlane::~FrustumPlane();

	void set3Points( Vector3 &v1,  Vector3 &v2,  Vector3 &v3);
	double distance(Vector3 &p);
};

}
#endif