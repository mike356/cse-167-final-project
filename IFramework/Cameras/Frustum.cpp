#include "Frustum.h"

#define ANG2RAD 3.14159265358979323846/180.0

namespace IFramework {

/// Variables
float Frustum::fov = 45, Frustum::nearD = 1, Frustum::farD = 20000;
float Frustum::nw = 0,Frustum::nh = 0,Frustum::fw = 0,Frustum::fh = 0;
Vector3 Frustum::ntl,Frustum::ntr,Frustum::nbl,Frustum::nbr,Frustum::ftl,Frustum::ftr,Frustum::fbl,Frustum::fbr;
FrustumPlane Frustum::pl[6];

int Frustum::drawn=0;

void Frustum::setSettings(float angle, float ratio, float near, float far) {

	float tang = (float)tan(angle* ANG2RAD * 0.5) ;
	nh = nearD * tang;
	nw = nh * ratio; 
	fh = farD  * tang;
	fw = fh * ratio;
}


void Frustum::setCamera(Vector3 &p, Vector3 &l, Vector3 &u) {

	Vector3 dir,nc,fc,X,Y,Z;

	Z = p - l;
	Z.normalize();

	X = u * Z;
	X.normalize();

	Y = Z * X;

	nc = p - Z * nearD;
	fc = p - Z * farD;

	ntl = nc + Y * nh - X * nw;
	ntr = nc + Y * nh + X * nw;
	nbl = nc - Y * nh - X * nw;
	nbr = nc - Y * nh + X * nw;

	ftl = fc + Y * fh - X * fw;
	ftr = fc + Y * fh + X * fw;
	fbl = fc - Y * fh - X * fw;
	fbr = fc - Y * fh + X * fw;

	pl[TOP].set3Points(ntr,ntl,ftl);
	pl[BOTTOM].set3Points(nbl,nbr,fbr);
	pl[LEFT].set3Points(ntl,nbl,fbl);
	pl[RIGHT].set3Points(nbr,ntr,fbr);
	pl[NEARP].set3Points(ntl,ntr,nbr);
	pl[FARP].set3Points(ftr,ftl,fbl);
}

int Frustum::pointInFrustum(Vector3 &p) {

	int result = INSIDE;
	for(int i=0; i < 6; i++) {

		if (pl[i].distance(p) < 0)
			return OUTSIDE;
	}
	return(result);

}

int Frustum::sphereInFrustum(Vector3 &p, float raio) {

	int result = INSIDE;
	float distance;
	for(int i=0; i < 6; i++) {
		distance = pl[i].distance(p);
		if (distance < -raio)
			return OUTSIDE;
		else if (distance < raio)
			result =  INTERSECT;
	}

	return(result);

}


int Frustum::boxInFrustum(BoundingBox &b) {

	int result = INSIDE;
	for(int i=0; i < 6; i++) {

		if (pl[i].distance(b.getVertexP(pl[i].normal)) < 0)
			return OUTSIDE;
		else if (pl[i].distance(b.getVertexN(pl[i].normal)) < 0)
			result =  INTERSECT;
	}
	return(result);

}



void Frustum::drawLines() {
#if 1
	float s = 0.999;

	Vector3 intl = ntl * s + fbr * (1.0 - s);
	Vector3 intr = ntr * s + fbl * (1.0 - s);
	Vector3 inbl = nbl * s + ftr * (1.0 - s);
	Vector3 inbr = nbr * s + ftl * (1.0 - s);
	Vector3 iftl = ftl * s + nbr * (1.0 - s);
	Vector3 iftr = ftr * s + nbl * (1.0 - s);
	Vector3 ifbl = fbl * s + ntr * (1.0 - s);
	Vector3 ifbr = fbr * s + ntl * (1.0 - s);

	// begin drawing inner frustum
	glColor3f(1,1,0);
	glBegin(GL_LINE_LOOP);
	//near plane
		glVertex3f(intl.x,intl.y,intl.z);
		glVertex3f(intr.x,intr.y,intr.z);
		glVertex3f(inbr.x,inbr.y,inbr.z);
		glVertex3f(inbl.x,inbl.y,inbl.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//far plane
		glVertex3f(iftr.x,iftr.y,iftr.z);
		glVertex3f(iftl.x,iftl.y,iftl.z);
		glVertex3f(ifbl.x,ifbl.y,ifbl.z);
		glVertex3f(ifbr.x,ifbr.y,ifbr.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//bottom plane
		glVertex3f(inbl.x,inbl.y,inbl.z);
		glVertex3f(inbr.x,inbr.y,inbr.z);
		glVertex3f(ifbr.x,ifbr.y,ifbr.z);
		glVertex3f(ifbl.x,ifbl.y,ifbl.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//top plane
		glVertex3f(intr.x,intr.y,intr.z);
		glVertex3f(intl.x,intl.y,intl.z);
		glVertex3f(iftl.x,iftl.y,iftl.z);
		glVertex3f(iftr.x,iftr.y,iftr.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//left plane
		glVertex3f(intl.x,intl.y,intl.z);
		glVertex3f(inbl.x,inbl.y,inbl.z);
		glVertex3f(ifbl.x,ifbl.y,ifbl.z);
		glVertex3f(iftl.x,iftl.y,iftl.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	// right plane
		glVertex3f(inbr.x,inbr.y,inbr.z);
		glVertex3f(intr.x,intr.y,intr.z);
		glVertex3f(iftr.x,iftr.y,iftr.z);
		glVertex3f(ifbr.x,ifbr.y,ifbr.z);

	glEnd();
	// end of drawing inner frustum
	glColor3f(0.5,0.5,0.5);
#else
	glBegin(GL_LINE_LOOP);
	//near plane
		glVertex3f(ntl.x,ntl.y,ntl.z);
		glVertex3f(ntr.x,ntr.y,ntr.z);
		glVertex3f(nbr.x,nbr.y,nbr.z);
		glVertex3f(nbl.x,nbl.y,nbl.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//far plane
		glVertex3f(ftr.x,ftr.y,ftr.z);
		glVertex3f(ftl.x,ftl.y,ftl.z);
		glVertex3f(fbl.x,fbl.y,fbl.z);
		glVertex3f(fbr.x,fbr.y,fbr.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//bottom plane
		glVertex3f(nbl.x,nbl.y,nbl.z);
		glVertex3f(nbr.x,nbr.y,nbr.z);
		glVertex3f(fbr.x,fbr.y,fbr.z);
		glVertex3f(fbl.x,fbl.y,fbl.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//top plane
		glVertex3f(ntr.x,ntr.y,ntr.z);
		glVertex3f(ntl.x,ntl.y,ntl.z);
		glVertex3f(ftl.x,ftl.y,ftl.z);
		glVertex3f(ftr.x,ftr.y,ftr.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	//left plane
		glVertex3f(ntl.x,ntl.y,ntl.z);
		glVertex3f(nbl.x,nbl.y,nbl.z);
		glVertex3f(fbl.x,fbl.y,fbl.z);
		glVertex3f(ftl.x,ftl.y,ftl.z);
	glEnd();

	glBegin(GL_LINE_LOOP);
	// right plane
		glVertex3f(nbr.x,nbr.y,nbr.z);
		glVertex3f(ntr.x,ntr.y,ntr.z);
		glVertex3f(ftr.x,ftr.y,ftr.z);
		glVertex3f(fbr.x,fbr.y,fbr.z);

	glEnd();
#endif
}

}
