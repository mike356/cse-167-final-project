#include "FrustumPlane.h"

namespace IFramework {

FrustumPlane::FrustumPlane( Vector3 &v1,  Vector3 &v2,  Vector3 &v3) {

	set3Points(v1,v2,v3);
}

FrustumPlane::FrustumPlane() {}

FrustumPlane::~FrustumPlane() {}

void FrustumPlane::set3Points( Vector3 &v1,  Vector3 &v2,  Vector3 &v3) {

	Vector3 aux1, aux2;

	aux1 = v1 - v2;
	aux2 = v3 - v2;

	normal = aux2 * aux1;

	normal.normalize();
	point =  v2;
	d = -(normal.dot(point));
}

double FrustumPlane::distance(Vector3 &p) {

	return (d + normal.dot(p));
}

}