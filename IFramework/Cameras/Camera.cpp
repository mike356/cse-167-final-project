#include "Camera.h"
#include <Utils\Utils.h>

namespace IFramework{

	/*
	Camera default settings, static variables
	*/
	int Camera::type = Camera::FREE;
	Vector3 Camera::speed_dir(0,0,0);
	Vector3 Camera::rotation(0,0,0);
	Vector3 Camera::position(0,0,20);
	Vector3 Camera::target(0,0,-1);
	Vector3 Camera::right(1,0,0);
	Vector3 Camera::up(0,1,0);
	double Camera::speed_factor = 10;

	bool Camera::moveFoward  = false;
	bool Camera::moveBackward  = false;
	bool Camera::moveLeft  = false;
	bool Camera::moveRight = false;

	int Camera::position_index = 1;
	int Camera::bezierDivisions = 10;
	Vector3 Camera::bezier_direction = Vector3(0,0,0);
	double Camera::bezier_distance = 0;
	double Camera::bezier_traveled = 0;
	Bezier* Camera::cameraPositions;

	int Camera::target_index = 1;
	Vector3 Camera::bezier_target_direction = Vector3(0,0,0);
	double Camera::bezier_target_distance = 0;
	double Camera::bezier_target_traveled = 0;
	Bezier* Camera::cameraTargets;


	bool Camera::bezier_forward = true;

	void Camera::update(){

		switch(type){

		case FREE:
			calculateFreeCamera();
			
		Frustum::setCamera(position,position+(target),Vector3(0,1,0));

			break;
		case BEZIER:
			calculateBezierCamera();

		Frustum::setCamera(position,target,Vector3(0,1,0));

			break;
		}


		if(cameraPositions!=NULL && cameraPositions->getVertices()->size()>1){
			cameraPositions->render();
			cameraTargets->render();
		}
	}

	void Camera::keyDown(unsigned char key, int x, int y){

		switch(key){
		case 'w':
			moveFoward = true;
			break;
		case 's':
			moveBackward = true;
			break;
		case 'a':
			moveLeft = true;
			break;
		case 'd':
			moveRight = true;
			break;
		case 'f':
			type = Camera::FREE;
			break;
		case 'b':
			type = Camera::BEZIER;
			break;
		case 'q':
			bezier_forward = true;
			break;
		case 'e':
			bezier_forward = false;
			break;
		case '-':
			speed_factor-=0.01;
			Utils::limit(speed_factor,0,10);
			break;
		case '=':
			speed_factor+=0.01;
			Utils::limit(speed_factor,0,10);
			break;

		}
	}

	void Camera::keyUp(unsigned char key, int x, int y){

		switch(key){
		case 'w':
			moveFoward = false;
			break;
		case 's':
			moveBackward = false;
			break;
		case 'a':
			moveLeft = false;
			break;
		case 'd':
			moveRight = false;
			break;
		}
	}


	void Camera::mouseInput(int x, int y){
		if(type==FREE){
		static bool warp = false;
		if(!warp) {

			double deltaX = x - glutGet(GLUT_WINDOW_WIDTH)/2;
			double deltaY  = y - glutGet(GLUT_WINDOW_HEIGHT)/2;

			rotation.x += deltaY / 10;
			rotation.y += deltaX / 10;

			Utils::limit(rotation.x,-90,90);

			if (rotation.y < -180.0f)
			{
				rotation.y += 360.0f;
			}

			if (rotation.y > 180.0f)
			{
				rotation.y -= 360.0f;
			}

			Utils::anglesToAxes(rotation,right,up,target);

			glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);
			warp = true;
		} else {
			warp = false;
		}
		}
	}

	void Camera::calculateFreeCamera(){

		position += speed_dir;

		glRotatef(rotation.x, 1.0f, 0.0f, 0.0f); 
		glRotatef(rotation.y, 0.0f, 1.0f, 0.0f); 
		glTranslatef(-position.x,-position.y,-position.z);

		speed_dir.x = 0;
		speed_dir.y = 0;
		speed_dir.z = 0;

		if (moveFoward)
		{
			// Control X-Axis movement
			double pitchFactor = cos(Utils::degreeToRad(rotation.x));
			speed_dir.x += ( speed_factor * double(sin(Utils::degreeToRad(rotation.y))) ) * pitchFactor;

			// Control Y-Axis movement
			speed_dir.y += speed_factor * double(sin(Utils::degreeToRad(rotation.x))) * -1.0f;

			// Control Z-Axis movement
			double yawFactor = double(cos(Utils::degreeToRad(rotation.x)));
			speed_dir.z += ( speed_factor * double(cos(Utils::degreeToRad(rotation.y))) * -1.0f ) * yawFactor;
		}

		if (moveBackward)
		{
			// Control X-Axis movement
			double pitchFactor = cos(Utils::degreeToRad(rotation.x));
			speed_dir.x += ( speed_factor * double(sin(Utils::degreeToRad(rotation.y))) * -1.0f) * pitchFactor;

			// Control Y-Axis movement
			speed_dir.y += speed_factor * double(sin(Utils::degreeToRad(rotation.x)));

			// Control Z-Axis movement
			double yawFactor = double(cos(Utils::degreeToRad(rotation.x)));
			speed_dir.z += ( speed_factor * double(cos(Utils::degreeToRad(rotation.y))) ) * yawFactor;
		}

		if (moveLeft)
		{
			double yRotRad = Utils::degreeToRad(rotation.y);

			speed_dir.x += -speed_factor * double(cos(yRotRad));
			speed_dir.z += -speed_factor * double(sin(yRotRad));
		}

		if (moveRight)
		{
			double yRotRad = Utils::degreeToRad(rotation.y);

			speed_dir.x += speed_factor * double(cos(yRotRad));
			speed_dir.z += speed_factor * double(sin(yRotRad));
		}

		Utils::limit(speed_dir.x,-speed_factor,speed_factor);
		Utils::limit(speed_dir.y,-speed_factor,speed_factor);
		Utils::limit(speed_dir.z,-speed_factor,speed_factor);
	}

	void Camera::createBezierCamera(vector<Vector3> positions,vector<Vector3> targets){
		cameraPositions = new Bezier(positions,bezierDivisions);
		cameraTargets = new Bezier(targets,bezierDivisions);
	}

	void Camera::calculateBezierCamera(){

		//glRotatef(rotation.x, 1.0f, 0.0f, 0.0f); 
		//glRotatef(rotation.y, 0.0f, 1.0f, 0.0f); 
		//glTranslatef(-position.x,-position.y,-position.z);
		gluLookAt(position.x,position.y,position.z,
				  target.x, target.y, target.z ,
				  0, 1, 0);

		Frustum::setCamera(position,target,up);

		if(bezier_forward){

			if(position_index>=cameraPositions->getVertices()->size()){
				///position_index = 1;
			}else{
				if(bezier_traveled>=bezier_distance){

					Vector3 a = *cameraPositions->getVertices()->at(position_index-1);
					Vector3 b = *cameraPositions->getVertices()->at(position_index);
					bezier_distance = (b - a).length();
					bezier_direction = ( b - a);
					bezier_direction.normalize();

					bezier_traveled = 0;
					position = a;

					++position_index;
				}else{
					bezier_traveled += speed_factor;
					position += bezier_direction*speed_factor;
				}
			}
			
			if(target_index>=cameraTargets->getVertices()->size()){
				//target_index = 1;
			}else{
				if(bezier_target_traveled>=bezier_target_distance){

					Vector3 a = *cameraTargets->getVertices()->at(target_index-1);
					Vector3 b = *cameraTargets->getVertices()->at(target_index);
					bezier_target_distance = (b - a).length();
					bezier_target_direction = ( b - a);
					bezier_target_direction.normalize();

					bezier_target_traveled = 0;
					target = a;

					++target_index;
				}else{
					bezier_target_traveled += speed_factor;
					target += bezier_target_direction*speed_factor;
				}
			}

		}else{

			if(position_index<=0){
				///position_index = bezierDivisions*2;
			}else{

				if(bezier_traveled>=bezier_distance){

					Vector3 a = *cameraPositions->getVertices()->at(position_index);
					Vector3 b = *cameraPositions->getVertices()->at(position_index-1);
					bezier_distance = (b - a).length();
					bezier_direction = ( b - a);
					bezier_direction.normalize();

					bezier_traveled = 0;
					position = a;

					--position_index;
				}else{
					bezier_traveled += speed_factor;
					position += bezier_direction*speed_factor;
				}
			}

			if(target_index<=0){
				///position_index = bezierDivisions*2;
			}else{

				if(bezier_target_traveled>=bezier_target_distance){

					Vector3 a = *cameraTargets->getVertices()->at(target_index);
					Vector3 b = *cameraTargets->getVertices()->at(target_index-1);
					bezier_target_distance = (b - a).length();
					bezier_target_direction = ( b - a);
					bezier_target_direction.normalize();

					bezier_target_traveled = 0;
					target = a;

					--target_index;
				}else{
					bezier_target_traveled += speed_factor;
					target += bezier_target_direction*speed_factor;
				}
			}

		}

	}

}
