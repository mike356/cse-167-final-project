#ifndef CAMERA_H_
#define CAMERA_H_

#include <stdlib.h>
#include <math.h>
#include <Core\Math\Matrix4.h>
#include "Frustum.h"
#include <Core\Geometry\Bezier.h>
#include <iostream>
#include <GL/glut.h>

using namespace std;

namespace IFramework{

class Camera{

 private:
  static Vector3 rotation,speed_dir;
  static Vector3 target,right,up;

  static void calculateFreeCamera();
  static bool moveFoward;
  static bool moveBackward;
  static bool moveLeft;
  static bool moveRight;

  static void calculateBezierCamera();
  static int position_index;
  static int bezierDivisions;
  static Vector3 bezier_direction;
  static double bezier_distance;
  static double bezier_traveled;
  static bool bezier_forward;

  static int target_index;
  static Vector3 bezier_target_direction;
  static double bezier_target_distance;
  static double bezier_target_traveled;
  static Bezier* cameraTargets;

 public:
  static double speed_factor;
  static Vector3 position;
  static int type;
  static Bezier* cameraPositions;

  enum MODE{
	  FREE,
	  BEZIER
  };

  static void update();
  static void lookAt(Vector3 pos,Vector3 at, Vector3 up);
  static void keyDown(unsigned char key, int x, int y);
  static void keyUp(unsigned char key, int x, int y);
  static void mouseInput(int x, int y);

  static void createBezierCamera(vector<Vector3> positions,vector<Vector3> targets);
  static void increaseSpeed();
  static void decreaseSpeed();

};

}
#endif 