#ifndef _Water_H_
#define _Water_H_

#include <Scenes\Geode.h>
#include <Textures\TextureManager.h>

namespace IFramework {

class Water : public Geode
{
  private:
	GLint tangentLocation;
	GLuint colorTexId;
	GLuint normalMapId;
	double water_pos;

  public:
    Water();
    Water(int size, int grid_size, double x,double y,double z,GLuint colorMap, GLuint normalMap);
	int size, grid_size;

	void calculateBounding();
    void render();
};

}

#endif