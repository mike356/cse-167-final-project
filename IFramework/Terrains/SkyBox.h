#ifndef SKYBOX_H
#define SKYBOX_H


#include <Opengl_libs.h>

#include "Textures\TextureManager.h"
#include "Shaders\ShaderManager.h"
#include "Utils\bmpLoader.h"
#include "Utils\Utils.h"

namespace IFramework{

class SkyBox 
{
private:
    unsigned int vaoHandle;
	static GLenum targets[6];
	static const char * suffixes[6];
public:
	static GLuint texID;
	static Shader* shader;

    SkyBox();
	SkyBox::~SkyBox();
	void init();
	void load( const char * baseFileName);
    void render();

};


}

#endif