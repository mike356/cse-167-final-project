/*BoidList object class
* Matt Wetmore
* Changelog
* ---------
* 12/18/09: Started work
*/

#include <stdlib.h>
#include <vector>
#include "Boid.cpp"

namespace IFramework{

class BoidList
{
public:
  vector<Boid*> boids; //will hold the boids in this BoidList
  double h; //for color
   
  BoidList(int n,double ih)
  {
    boids = vector<Boid*>();
    h = ih;
    for(int i=0;i<n;i++)
		boids.push_back(new Boid(Vector3(0,0,0))  );
  }
   
  void add()
  {
	  boids.push_back(new Boid(Vector3(width/2,height/2,600)));
  }
   
  void addBoid(Boid* b)
  {
	  boids.push_back(b);
  }
   
  void run(bool aW)
  {
    for(int i=0;i<(int)boids.size();i++) //iterate through the list of boids
    {
      Boid* tempBoid = boids.at(i); //create a temporary boid to process and make it the current boid in the list
      tempBoid->h = h;
      tempBoid->avoidWalls = aW;
      tempBoid->run(boids); //tell the temporary boid to execute its run method
    }
  }
   
  Boid* getBoid(int n)
  {
    if(n<(int)boids.size())
      return boids.at(n);
    return NULL;
  }
   
  void remove(int n)
  {
    if(n<(int)boids.size())
      boids.erase(boids.begin() + n);
  }
   
  void remove()
  {
    if((int)boids.size()>0)
      //boids.remove(boids.size()-1);
	  boids.clear();
  }
};

}
