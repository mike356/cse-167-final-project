#ifndef _MATRIX4_H_
#define _MATRIX4_H_
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include "Vector4.h"
#include "Vector3.h"

namespace IFramework {

class Matrix4
{
protected:
  public:
	double m[4][4];   // matrix elements
    Matrix4();        // constructor
    Matrix4(double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double);
    double* getPointer();  // return pointer to matrix elements
    void identity();  // create identity matrix
	
	Matrix4 operator*(Matrix4 &a);
    void operator*=(Matrix4 &a);	
	Vector4 operator*(Vector4 &a);
	
	void set(int row, int col, int value);

	void rotateX(double);
	void rotateY(double);
	void rotateZ(double);
	void rotate(double angle, double x, double y, double z);
	void scale(double,double,double);
	void translate(double,double,double);
	void print();
	Matrix4 clone();
	void transpose();
};

}

#endif