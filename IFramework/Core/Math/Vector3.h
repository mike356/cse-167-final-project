#ifndef _VECTOR3_H_
#define _VECTOR3_H_

#include "math.h"
#include <iostream>
using namespace std;

namespace IFramework {

class Vector3 {
public:
	double x,y,z;
	Vector3();
	Vector3(double x0,double y0,double z0);
	Vector3(const Vector3 &a);

	void set(double x0,double y0,double z0);
	double get(int);

	/// Operators
	Vector3 & operator=(const Vector3&a);
	Vector3 operator+(Vector3& );
	void operator+=(Vector3& );
	Vector3 operator-(Vector3&);
	void operator-=(Vector3& );

	double operator[](int);
	bool operator==(const Vector3&a);
	bool operator!=(const Vector3&a);
	Vector3 operator*(double a);
	void operator*=(double a);
	Vector3 operator*(Vector3&a);
	Vector3 operator/(double s);

	/// Methods
	Vector3 add(Vector3& a);
	Vector3 sub(Vector3& a);
	Vector3 mult(double s);
	Vector3 div(double s);
	double dot(Vector3& a);
	Vector3 cross(Vector3&);
	void cross(Vector3& a,Vector3& b);
	double length();
	void normalize();
	void print();
	double angle(Vector3&a);
	Vector3 clone();
	void limit(double max);

	static Vector3 normal(Vector3 a, Vector3 b, Vector3 c);
};

}
#endif